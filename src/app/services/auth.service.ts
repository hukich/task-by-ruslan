import { Injectable } from '@angular/core';
import {User} from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly usersUrl = 'http://localhost:3000/users';

  constructor(private http: HttpClient) { }

  loginUser(user: User): Observable<any>{
    return this.http.get(this.usersUrl);

  }

  register(user: User): Observable<any>{
    return this.http.post<User>(this.usersUrl, user);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  deleteUser(id: number): Observable<{}> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.delete(url);

  }
}

