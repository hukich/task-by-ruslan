import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'app/services/auth.service';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseConfig } from 'app/fuse-config/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  users;
  loginForm: FormGroup;

  constructor(private authService: AuthService,
              private route: Router,
              private _fuseConfigService: FuseConfigService,
              private fb: FormBuilder
              ) {
                this._fuseConfigService.config = fuseConfig;
               }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: null,
      password: null
    });
  }

  loginUser() {
    const user = this.loginForm.value;
    console.log(user);
    this.authService.loginUser(user).subscribe(data => {
      const findUser  = data.find((userFind)=> {
        return userFind.login === user.login && userFind.password === user.password;
      })
      if(findUser){
        this.route.navigate(['/sample'])
      }
    })
    ;
    
  }

}
