import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import { AuthService } from 'app/services/auth.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseConfig } from 'app/fuse-config';
import { User } from 'app/models/user';



@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class SampleComponent implements OnInit{
    masUsers;
    users = [];
    displayedColumns: string[] = ['login', 'password', 'buttons'];
    dataSource: MatTableDataSource<User>;



    @ViewChild(MatSort, {static: true}) sort: MatSort;

    
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * 
     * 
     */
    ngOnInit() {
        
    }
    
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private authService: AuthService,
        private _fuseConfig: FuseConfigService
    )
    {
        this._fuseConfig.config = fuseConfig;
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
        
        this.authService.getUsers().subscribe(data => {
            console.log(data);
            //this.users = User;
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.sort = this.sort;
        })
        
    }
    
    
    deleteUser(user) {
        this.authService.deleteUser(user.id).subscribe(()=> {
            this.users = this.users.filter(u => u.id !== user.id)
        })
      }
}
