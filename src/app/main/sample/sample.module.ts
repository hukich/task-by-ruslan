import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { SampleComponent } from './sample.component';
import { MatTableModule } from '@angular/material/table';

import { MatSortModule } from '@angular/material/sort';

import { MatButtonModule } from '@angular/material/button';

import { MatIconModule } from '@angular/material/icon';

import {MatTooltipModule} from '@angular/material/tooltip';



@NgModule({
    declarations: [
        SampleComponent
    ],
    imports     : [
    

        TranslateModule,

        FuseSharedModule,
        MatTableModule,
        MatSortModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule
    ],
    exports     : [
        SampleComponent
    ]
})

export class SampleModule
{
}
