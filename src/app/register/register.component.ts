import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'app/services/auth.service';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseConfig } from 'app/fuse-config/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private authServise: AuthService,
              private route: Router,
              private fb: FormBuilder,
              private _fuseConfigService: FuseConfigService) {
                this._fuseConfigService.config = fuseConfig;
               }

  ngOnInit() {
    this.registerForm = this.fb.group({
      login: null,
      password: null
    });
  }
  registerUser(userData: NgForm) {
    const user = this.registerForm.value;

    this.authServise.register(user).subscribe(user => {
      console.log(user);

    })
    this.route.navigate(["/login"])


  }
}
